import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import firebase from 'firebase';

import {Header, Button, Spinner} from './components/common';
import LoginForm from './components/LoginForm';

export default class App extends Component {
    state = {loggedIn: null};

    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyAVfNxO_y15XtiooUL1sMEVV_5cJXHMSAI',
            authDomain: 'authentication-6c3b3.firebaseapp.com',
            databaseURL: 'https://authentication-6c3b3.firebaseio.com',
            projectId: 'authentication-6c3b3',
            storageBucket: 'authentication-6c3b3.appspot.com',
            messagingSenderId: '212909448171'
        };
        firebase.initializeApp(config);

        firebase.auth().onAuthStateChanged(user => {
            this.setState({loggedIn: (user !== null)});
        });
    }

    renderContent() {
        switch (this.state.loggedIn) {
            case true:
                return <View style={{flexDirection: 'row'}}><Button onPress={() => firebase.auth().signOut()}>Log out</Button></View>;
                break;
            case false:
                return <LoginForm/>;
                break;
            default:
                return <View style={{flexDirection: 'row'}}><Spinner/></View>
        }
    }

    render() {
        return (
            <View style={styles.App}>
                <Header caption="Authentication" />
                {this.renderContent()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    App: {
        //flex: 1
    }
});