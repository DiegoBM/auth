import React from 'react';
import {TextInput, View, Text, StyleSheet} from 'react-native';

const Input = ({label, value, placeholder, secureTextEntry, onChangeText}) => (
    <View style={styles.Input}>
        <Text style={styles.textLabel}>{label}</Text>
        <TextInput
            secureTextEntry={secureTextEntry}
            autoCorrect={false}
            placeholder={placeholder || ''}
            onChangeText={onChangeText}
            value={value}
            style={styles.textInput}/>
    </View>
);

const styles = StyleSheet.create({
    Input: {
        flexDirection: 'row',
        height: 40,
        flex: 1,
        alignItems: 'center'
    },
    textLabel: {
        fontSize: 18,
        paddingLeft: 20,
        flex: 1
    },
    textInput: {
        color: '#000',
        paddingRight: 5,
        paddingLeft: 5,
        fontSize: 18,
        lineHeight: 23,
        flex: 3
    }
});

export {Input};