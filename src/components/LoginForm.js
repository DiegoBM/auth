import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';
import firebase from 'firebase';

import {Card, CardSection, Button, Input, Spinner} from './common';

export default class LoginForm extends Component {
    state = {email: '', password: '', error: '', loading: false};

    handleRequestSuccess = () => {
        console.log('success');
        this.setState({email: '', password: '', loading: false});
    };

    handleButtonPress = () => {
        const {email, password} = this.state;

        this.setState({error: '', loading: true});

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(this.handleRequestSuccess)
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then(this.handleRequestSuccess)
                    .catch(() => this.setState({error: 'Authentication failed', loading: false}));
            });
    };

    renderButton() {
        if (this.state.loading) {
            return <Spinner size="small"/>
        } else {
            return <Button onPress={this.handleButtonPress}>Log in</Button>
        }
    }

    render() {
        return (
            <Card>
                <CardSection>
                    <Input
                        placeholder="user@email.com"
                        label="Email"
                        value={this.state.email}
                        onChangeText={email => this.setState({email})} />
                </CardSection>
                <CardSection>
                    <Input
                        secureTextEntry
                        placeholder="password"
                        label="Password"
                        value={this.state.password}
                        onChangeText={password => this.setState({password})} />
                </CardSection>
                <Text style={styles.error}>{this.state.error}</Text>
                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>
        );
    }
}

const styles = StyleSheet.create({
    error: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red',
    }
});